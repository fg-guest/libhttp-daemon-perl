Source: libhttp-daemon-perl
Section: perl
Priority: optional
Build-Depends: debhelper (>= 10)
Build-Depends-Indep: perl,
 libhttp-date-perl,
 libhttp-message-perl,
 liblwp-mediatypes-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders:
Standards-Version: 4.1.4
Homepage: https://metacpan.org/release/HTTP-Daemon
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libhttp-daemon-perl.git
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libhttp-daemon-perl

Package: libhttp-daemon-perl
Architecture: all
Depends: ${misc:Depends}, ${perl:Depends},
 libhttp-date-perl,
 libhttp-message-perl,
 liblwp-mediatypes-perl
Replaces: libwww-perl (<< 6.00)
Breaks: libwww-perl (<< 6.00)
Description: simple http server class
 Instances of the HTTP::Daemon class are HTTP/1.1 servers that listen on a
 socket for incoming requests. The HTTP::Daemon is a subclass of
 IO::Socket::INET, so you can perform socket operations directly on it too.
 .
 The accept() method will return when a connection from a client is available.
 The returned value will be an HTTP::Daemon::ClientConn object which is
 another IO::Socket::INET subclass. Calling the get_request() method on this
 object will read data from the client and return an HTTP::Request object. The
 ClientConn object also provide methods to send back various responses.
 .
 This HTTP daemon does not fork(2) for you. Your application, i.e. the user of
 the HTTP::Daemon is responsible for forking if that is desirable. Also note
 that the user is responsible for generating responses that conform to the
 HTTP/1.1 protocol.
